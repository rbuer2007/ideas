package de.buechner.ideas.services;

import org.junit.Test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;

public class BonusBTest {
    @Test
    public void testChangeFooBar() {
        TestClass testClass = new TestClass();
        assertEquals("test", testClass.getFoobar());
        testClass=spy(testClass);
        when(testClass.getFoobar()).thenReturn("SUCCESS");
        assertEquals("SUCCESS", testClass.getFoobar());
    }
}
