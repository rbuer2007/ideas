package de.buechner.ideas.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddServiceTest {
    @Test
    public void test() {
        String s = "1";
        Integer i = 1;
        s = add(s, 1);
        assertEquals("2", s);
        s = add(s, 5);
        assertEquals("7", s);
        i = add(i, 2);
        assertEquals((Integer) 3, i);
        i = add(i, 1);
        assertEquals((Integer) 4, i);
    }

    private <T> T add(T input, int i) {
        final String stringValue = String.valueOf(input);
        final Integer intValue = Integer.valueOf(stringValue);
        final Integer intResult = Integer.valueOf(intValue.intValue() + i);
        if(input.getClass().equals(Integer.class))
            return (T)intResult;
        if(input.getClass().equals(String.class))
            return (T)intResult.toString();
        else throw new IllegalArgumentException("Only String and Integer are supported for input");
    }
}
