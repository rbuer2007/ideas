package de.buechner.ideas.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {CharGrouper.class})
@RunWith(SpringRunner.class)
public class CharGrouperMockTest {

    private static final String SPECIAL_ANSWER = "SUCCSESS";
    private static final String MAGIC_STRING = "abcdef";
    @MockBean
    private ArraySorter arraySorter;
    @SpyBean
    private StringCompressor stringCompressor;
    @Autowired
    private CharGrouper charGrouper;

    @Before
    public void setup(){
        doReturn((new int[]{})). //
                when(arraySorter).sort(any(int[].class));
        doReturn(SPECIAL_ANSWER.chars().toArray()). //
                when(arraySorter).sort(MAGIC_STRING.chars().toArray());
    }

    /**
     * assert that {@link CharGrouper#group(String)} with a String that differs from SPECIAL_ANSWER returns ""
     */
    @Test
    public void group_example_returnsEmpty(){
        assertThat(charGrouper.group("a4bins2uz")).isEmpty();
        // cannot verify charGrouper because it is not (and should not be) a mock or spy
        // makes more sense to verify that StringCompressor is used
        verify(stringCompressor).compress(eq(""));
    }

    /**
     * assert that {@link CharGrouper#group(String)} with #MAGIC_STRING returns SPECIAL_ANSWER
     */
    @Test
    public void group_magicString_returnsSUCCESS(){
        assertThat(charGrouper.group(MAGIC_STRING)).isEqualTo(SPECIAL_ANSWER);
        // cannot verify charGrouper because it is not (and should not be) a mock or spy
        // makes more sense to verify that StringCompressor is used
        verify(stringCompressor).compress(eq(SPECIAL_ANSWER));
    }
}