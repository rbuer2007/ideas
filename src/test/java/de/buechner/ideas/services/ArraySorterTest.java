package de.buechner.ideas.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest(classes = {ArraySorter.class})
@RunWith(SpringRunner.class)
public class ArraySorterTest {

    @Autowired
    private ArraySorter arraySorter;

    /**
     * asserts that <<code>null</code> as Input throws {@link NullPointerException}
     * due to missing declaration
     * special case empty array
     */
    @Test
    public void sortNull_ExpectNPE() {
        assertThatThrownBy(()->arraySorter.sort(null)).isInstanceOf(NullPointerException.class);
    }

    /**
     * asserts that an Empty input returns empty clone
     * to ensure clone is used and input is not changed
     * special case empty array
     */
    @Test
    public void sortEmpty_ExpectNotSameButEmptyResult() {
        int[] input=new int[0];
        assertThat(arraySorter.sort(input)). //
                isNotSameAs(input). //
                isEmpty();
        assertThat(input).isEmpty();
    }
    /**
     * asserts that sorted array return another array with same elements in same ascending order
     * special case sorted array
     */
    @Test
    public void sortSorted_ExpectNotSameButSortedResult() {
        int[] input={-1,0,23,42};
        int[] expected={-1,0,23,42};
        assertThat(arraySorter.sort(input)). //
                isNotSameAs(input). //
                isNotSameAs(expected). //
                containsExactly(expected);
        assertThat(input). //
                isNotSameAs(expected). //
                containsExactly(expected);
    }

    /**
     * asserts that unsorted array returns another array with same elements but sorted ascending
     * default case
     */
    @Test
    public void sortUnSorted_ExpectNotSameButSortedResult() {
        int[] input={42,0,-1,23};
        int[] expected={-1,0,23,42};
        assertThat(arraySorter.sort(input)). //
                isNotSameAs(input). //
                isNotSameAs(expected). //
                containsExactly(expected);
        assertThat(input). //
                isNotSameAs(expected). //
                containsExactly(42,0,-1,23);
    }

}