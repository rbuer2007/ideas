package de.buechner.ideas.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest(classes = {CharGrouper.class, ArraySorter.class, StringCompressor.class})
@RunWith(SpringRunner.class)
public class CharGrouperTest {

    @Autowired
    private CharGrouper charGrouper;
    @Test
    public void example(){
        assertEquals("a4bins2uz", charGrouper.group("abzuaaissna"));
    }
}