package de.buechner.ideas.rest;

import de.buechner.ideas.services.ArraySorter;
import de.buechner.ideas.services.CharGrouper;
import de.buechner.ideas.services.StringCompressor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = {ArraySorter.class, StringCompressor.class, CharGrouper.class, CharGrouperController.class})
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CharGrouperCharGrouperControllerTest {

    @Autowired
    MockMvc mockMvc;

    /**
     * tests GET Request for {@link CharGrouperController#PATH}
     * @throws Exception
     */
    @Test
    public void example() throws Exception {
        mockMvc.perform( //
                get(CharGrouperController.PATH). //
                param(CharGrouperController.INPUT,"abzuaaissna")). //
                andExpect(status().isOk()). //
                andExpect(content().string("a4bins2uz")
        );
    }
}