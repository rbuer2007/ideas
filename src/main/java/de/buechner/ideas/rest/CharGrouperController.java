package de.buechner.ideas.rest;

import de.buechner.ideas.services.CharGrouper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Controller to access {@link CharGrouper}
 */
@RestController()
@RequestMapping(CharGrouperController.PATH)
public class CharGrouperController {
    protected static final String PATH = "/group";
    protected static final String INPUT = "input";
    @Autowired
    private CharGrouper charGrouper;

    /**
     * {@link GetMapping} for {@link CharGrouper#group(String)}
     */
    @GetMapping(produces = "text/plain")
    public String group(@RequestParam(value = INPUT,required = false) String input){
            return isEmpty(input)?"":charGrouper.group(input);
    }
}
