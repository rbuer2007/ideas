package de.buechner.ideas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Service that sorts and compress Strings
 */
@Service
public class CharGrouper {
    @Autowired
    private ArraySorter arraySorter;
    @Autowired
    private StringCompressor stringCompressor;

    /**
     * first sorts {@link ArraySorter#sort(int[])} then compresses {@link StringCompressor#compress(String)}  the given input
     * converts to cha
     * @param input
     * @return
     */
    public String group(String input){

        int[] sortedInts = arraySorter.sort(input.chars().toArray());

        char[] chars = toChars(sortedInts);

        return stringCompressor.compress(String.copyValueOf(chars));
    }

    private char[] toChars(int[] sortedInts) {
        char[] chars= new char[sortedInts.length];
        for (int i=0; i<sortedInts.length;i++) {
            chars[i]=(char)sortedInts[i];
        }
        return chars;
    }
}
