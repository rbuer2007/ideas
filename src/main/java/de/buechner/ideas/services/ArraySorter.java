package de.buechner.ideas.services;

import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Arraysorter for Task002
 */
@Service
public class ArraySorter {
    /**
     * the given sort function
     * @param unsorted the unsorted Array
     * @return sorted clone of given array
     */
    public int[] sort(int[] unsorted) {
        int[] copy = Arrays.copyOf(unsorted, unsorted.length);
        Arrays.sort(copy);
        return copy;
    }
}
