package de.buechner.ideas.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static java.lang.Character.forDigit;

@Service
public class StringCompressor {
    public String compress(String uncompressed) {
        if(StringUtils.length(uncompressed)<=1) {
            return uncompressed;
        }
        StringBuilder result=new StringBuilder();
        char currentChar=uncompressed.charAt(0);
        int currentCharCount=1;
        for(int i=1; i<uncompressed.length();i++){
            char charAt = uncompressed.charAt(i);
            if(charAt == currentChar){
                currentCharCount++;
            }else{
                result.append(getToAppend(currentChar,currentCharCount));
                currentChar=charAt;
                currentCharCount=1;
            }
        }
        //append the last character
        result.append(getToAppend(currentChar,currentCharCount));
        if(result.length()==uncompressed.length()){
            return uncompressed;
        }
        return result.toString();
    }

    private char[] getToAppend(char currentChar, int count) {
        return count > 1 ? new char[]{currentChar, forDigit(count,10)} : new char[]{currentChar};
    }
}
